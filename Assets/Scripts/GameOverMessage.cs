using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameOverMessage : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI gameOverText;

    // Start is called before the first frame update
    void Start()
    {
        if(StaticStorage.winState == WinState.OutOfAir)
        {
            gameOverText.text = "Game Over!\nYou ran out of air, but you managed to make it " + StaticStorage.stagesComplete + " stages. Remember to keep picking up those air bubbles, they're crucial to your survival.\nThanks for playing.";
        }
        else if (StaticStorage.winState == WinState.AllStagesComplete)
        {
            gameOverText.text = "Well Done!\nYou managed to complete all the stages currently in the game.\nThanks for playing.";
        }
        else if (StaticStorage.winState == WinState.Dead)
        {
            gameOverText.text = "Game Over!\nYou took too much damage.\nThanks for playing.";
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
