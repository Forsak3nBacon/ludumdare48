using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayAgainBtn : MonoBehaviour
{
    public void PlayAgain()
    {
        StaticStorage.stagesComplete = 0;
        StaticStorage.winState = WinState.GameRunning;

        SceneManager.LoadScene(1);

    }
}
