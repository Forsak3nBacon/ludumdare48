using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicPlayer : MonoBehaviour
{
    [SerializeField] AudioSource music;
    [SerializeField] Sprite uncheckedIcon;
    [SerializeField] Sprite checkedIcon;
    [SerializeField] GameObject buttonGameObject;

    bool audioMuted = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        buttonGameObject = GameObject.Find("MuteMusic"); //To get the link between scenes.
    }

    public void ToggleMusic()
    {
        if (!audioMuted)
        {
            music.mute = true;
            audioMuted = true;
            buttonGameObject = GameObject.Find("MuteMusic"); //To get the link between scenes.
            buttonGameObject.GetComponent<Image>().sprite = checkedIcon;
        }
        else
        {
            music.mute = false;
            audioMuted = false;
            buttonGameObject = GameObject.Find("MuteMusic"); //To get the link between scenes.
            buttonGameObject.GetComponent<Image>().sprite = uncheckedIcon;
        }
    }
}
