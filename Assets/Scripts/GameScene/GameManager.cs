using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public  class GameManager : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (StaticStorage.winState == WinState.ScoreAchieved)
        {
            StaticStorage.winState = WinState.GameRunning;
            NextLevel();
        }
        else if (StaticStorage.winState == WinState.OutOfAir)
        {
            EndGame();
        }
        else if (StaticStorage.winState == WinState.AllStagesComplete)
        {
            EndGame();
        }
        else if (StaticStorage.winState == WinState.Dead)
        {
            EndGame();
        }
    }

    public void EndGame()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void NextLevel()
    {
        Scene scene = SceneManager.GetActiveScene();

        if (scene.name == "Level1")
        {
            SceneManager.LoadScene("Level2");
        }
        else if (scene.name == "Level2")
        {
            SceneManager.LoadScene("Level3");
        }
        else if (scene.name == "Level3")
        {
            StaticStorage.winState = WinState.AllStagesComplete;
        }
    }
}
