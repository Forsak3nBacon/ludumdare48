using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    [SerializeField] float healthTotal = 20f;

    protected float healthCurrent;

    // Start is called before the first frame update
    void Start()
    {
        healthCurrent = healthTotal;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public virtual bool TakeDamage(float damage)
    {
        healthCurrent -= damage;

        if (healthCurrent <= 0)
        {
            print(this.gameObject + " is dead.");

            return true; //Returning true tells the caller that it's dead.
        }

        return false;
    }

    public float GetHealth
    {
        get
        {
            return healthCurrent;
        }
    }
}
