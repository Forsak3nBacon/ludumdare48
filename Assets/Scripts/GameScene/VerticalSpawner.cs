using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalSpawner : Spawner
{
    protected override void Spawn()
    {
        GameObject spawned = Instantiate(prefabToSpawn);

        float spawnOffset = Random.Range(-8f, 8f); //Offset the spawn slightly so it's not always at the same spot.

        spawned.transform.position = new Vector3(transform.position.x + spawnOffset, transform.position.y);

        spawned.GetComponent<Jelly>().StartMoving();

        StartCoroutine(SpawnDelayStart());
    }
}
