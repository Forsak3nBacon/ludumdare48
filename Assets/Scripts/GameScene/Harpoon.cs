using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Harpoon : MonoBehaviour
{
    [SerializeField] float timeAlive = 5f;
    [SerializeField] float damage = 1f;
    [SerializeField] AudioSource hitSFX;

    [SerializeField] float cameraShakeIntensity;
    [SerializeField] float cameraShakeTime;



    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LifeSpan());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator LifeSpan()
    {
        yield return new WaitForSeconds(timeAlive);

        DestroyHarpoon();
    }

    void DestroyHarpoon()
    {
        Destroy(this.gameObject);
    }

    IEnumerator WaitToDestroyHarpoon()
    {
        //Disable the sprite and the render to allow for the hit sfx to play.
        GetComponent<SpriteRenderer>().enabled = false;
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        GetComponent<BoxCollider2D>().enabled = false;

        yield return new WaitForSeconds(1);
        DestroyHarpoon();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.CompareTag("Fish"))
        {
            collision.gameObject.GetComponent<Fish>().TakeDamage(damage);
            hitSFX.Play();
            CinemachineCameraShake.Instance.ShakeCamera(cameraShakeIntensity, cameraShakeTime);
            StartCoroutine(WaitToDestroyHarpoon());
        }

        if (collision.gameObject.CompareTag("Despawn")) //If hit bounding walls without dying, then delete it.
        {
            DestroyHarpoon();
        }
    }

    public void FlipSprite()
    {
        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
