using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StaticStorage
{
    public static int stagesComplete = 0;

    public static WinState winState = WinState.GameRunning;
}

public enum WinState
{
    GameRunning,
    ScoreAchieved,
    OutOfAir,
    AllStagesComplete,
    Dead
}
