using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jelly : Fish
{
    public override void StartMoving()
    {
        Vector3 velocity = new Vector3(0f, moveSpeed, 0f); //Vertical movement instead

        rb.velocity = velocity;
    }
}
