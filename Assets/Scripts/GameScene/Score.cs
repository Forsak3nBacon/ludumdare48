using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Score : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] float goalScore = 100f;
    [SerializeField] PlayerNotificationHandler notificationHandler;

    float score = 0f;

    bool scoreReached = false;

    // Start is called before the first frame update
    void Start()
    {
    
    }

    // Update is called once per frame
    void Update()
    {
        UpdateText();
        CheckWin();
    }

    public void IncreaseScore(float incScore)
    {
        score += incScore;
    }

    void UpdateText()
    {
        scoreText.text = score.ToString() + " / " + goalScore.ToString();
    }

    void CheckWin()
    {
        if(score >= goalScore)
        {
            notificationHandler.DisplayString("Goal reached - Come back to your vessel to progress.");
            scoreReached = true;
            /*StaticStorage.stagesComplete++;

            StaticStorage.winState = WinState.ScoreAchieved;*/
            //Next Level
        }
    }

    public bool ScoreReached
    {
        get
        {
            return scoreReached;
        }
    }
}
