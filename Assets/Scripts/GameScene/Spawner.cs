using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] protected GameObject prefabToSpawn = null;

    [SerializeField] protected float minSpawnDelay;
    [SerializeField] protected float maxSpawnDelay;

    bool leftSpawn = true;

    // Start is called before the first frame update
    void Start()
    {
        //Check set spawn delay meets rules.
        if(minSpawnDelay <= 0)
        {
            throw new System.Exception("Min Spawn Delay must be above 0");
        }
        if(maxSpawnDelay <= 0)
        {
            throw new System.Exception("Max Spawn Delay must be above 0");
        }
        if(maxSpawnDelay < minSpawnDelay)
        {
            throw new System.Exception("Max Spawn Delay must be above min spawn delay");
        }

        if (transform.position.x > 0)
        {
            leftSpawn = false;
        }
        else
        {
            leftSpawn = true;
        }

        StartCoroutine(SpawnDelayStart());
    }

    // Update is called once per frame
    void Update()
    {

    }

    protected IEnumerator SpawnDelayStart()
    {
        float spawnDelay = Random.Range(minSpawnDelay, maxSpawnDelay);

        yield return new WaitForSeconds(spawnDelay);

        Spawn();
    }

    protected virtual void Spawn()
    {
        GameObject spawned = Instantiate(prefabToSpawn);

        float spawnOffset = Random.Range(-1f, 1f); //Offset the spawn slightly so it's not always at the same spot.

        spawned.transform.position = new Vector3(transform.position.x, transform.position.y + spawnOffset);

        if (!leftSpawn) //If spawning from right then flip.
        {
            spawned.GetComponent<Fish>().FlipSprite();
        }

        spawned.GetComponent<Fish>().StartMoving();

        StartCoroutine(SpawnDelayStart());
    }
}
