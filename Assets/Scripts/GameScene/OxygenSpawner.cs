using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OxygenSpawner : MonoBehaviour
{
    [SerializeField] float maxBubblesSpawnable = 4;
    [SerializeField] GameObject bubblePrefab = null;

    [SerializeField] TextMeshProUGUI bubbleCountText = null;

    [SerializeField] float minSpawnDelay;
    [SerializeField] float maxSpawnDelay;

    float bubblesRemaining;

    // Start is called before the first frame update
    void Start()
    {
        //Check set spawn delay meets rules.
        if (minSpawnDelay <= 0)
        {
            throw new System.Exception("Min Spawn Delay must be above 0");
        }
        if (maxSpawnDelay <= 0)
        {
            throw new System.Exception("Max Spawn Delay must be above 0");
        }
        if (maxSpawnDelay < minSpawnDelay)
        {
            throw new System.Exception("Max Spawn Delay must be above min spawn delay");
        }

        bubblesRemaining = maxBubblesSpawnable;

        Invoke("Spawn", 8f);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateText();
    }

    IEnumerator SpawnDelayStart()
    {
        float spawnDelay = Random.Range(minSpawnDelay, maxSpawnDelay);

        yield return new WaitForSeconds(spawnDelay);

        Spawn();
    }

    void Spawn()
    {
        if(bubblesRemaining > 0)
        {
            GameObject spawned = Instantiate(bubblePrefab);

            float spawnOffset = Random.Range(-8f, 8f); //Offset the spawn slightly so it's not always at the same spot.

            spawned.transform.position = new Vector3(transform.position.x + spawnOffset, transform.position.y);

            spawned.GetComponent<OxygenBubble>().StartMoving();

            bubblesRemaining--;

            StartCoroutine(SpawnDelayStart());
        }
    }

    void UpdateText()
    {
        if (bubblesRemaining == 1)
        {
            bubbleCountText.color = Color.yellow;
        }
        else if (bubblesRemaining == 0)
        {
            bubbleCountText.color = Color.red;
        }

        bubbleCountText.text = bubblesRemaining.ToString();
    }
}
