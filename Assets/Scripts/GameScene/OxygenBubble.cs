using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OxygenBubble : MonoBehaviour
{
    [SerializeField] float oxygenToGive = 20f;
    [SerializeField] Rigidbody2D rb = null;
    [SerializeField] float moveSpeed = 10f;

    Oxygen playerOxygen = null;

    // Start is called before the first frame update
    void Start()
    {
        playerOxygen = GameObject.Find("O2").GetComponent<Oxygen>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerOxygen.IncreaseOxygen(oxygenToGive);

            DestroyBubble();
        }
        if (collision.gameObject.CompareTag("Despawn")) //If hit bounding walls without dying, then delete it.
        {
            DestroyBubble();
        }
    }

    public void StartMoving()
    {
        Vector3 velocity = new Vector3(0f, moveSpeed, 0f);

        rb.velocity = velocity;
    }

    void DestroyBubble()
    {
        Destroy(this.gameObject);
    }
}
