using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour
{
    //Default Fish Script is used for Easy Level Fish. Harder level fish inherit this class and expand upon it.

    [SerializeField] protected float moveSpeed = 5f;
    
    [SerializeField] float fishWorth = 10f;
    
    [SerializeField] protected Rigidbody2D rb = null;
    Health health = null;

    Score scoreScript = null;

    bool isFacingRight = true;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        health = GetComponent<Health>();

        scoreScript = GameObject.Find("MoneyAmt").GetComponent<Score>();

        if(scoreScript == null)
        {
            throw new System.Exception("Fish could not find Score Script");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void StartMoving()
    {
        float localMoveSpeed;

        if (!isFacingRight) //Reverse the direction of travel
        {
            localMoveSpeed = -moveSpeed;
        }
        else
        {
            localMoveSpeed = moveSpeed;
        }

        Vector3 velocity = new Vector3(localMoveSpeed, 0f, 0f);

        rb.velocity = velocity;
    }

    public void FlipSprite()
    {
        isFacingRight = !isFacingRight;

        // Multiply the object's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Despawn")) //If hit bounding walls without dying, then delete it.
        {
            DestroyFish();
        }
    }

    public void TakeDamage(float damage)
    {
        if (health.TakeDamage(damage))
        {
            scoreScript.IncreaseScore(fishWorth);

            DestroyFish();
        }
    }

    protected void DestroyFish()
    {
        Destroy(this.gameObject);
    }
}
