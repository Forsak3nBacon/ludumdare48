using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [SerializeField] GameObject player = null;
    [SerializeField] float minHeight = 0f;
    float maxHeight; 


    // Start is called before the first frame update
    void Start()
    {
        maxHeight = transform.position.y; //Set to current gameobject height.
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(0, Mathf.Clamp(player.transform.position.y, minHeight, maxHeight), -10);
    }
}
