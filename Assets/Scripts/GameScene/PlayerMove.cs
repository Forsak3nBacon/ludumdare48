using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] float moveSpeed = 5f;

    [SerializeField] Animator animator;

    Vector3 velocity;

    bool isFacingRight = true;

    Rigidbody2D rb = null;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerInput();
    }

    private void FixedUpdate()
    {
        Move();
    }

    void PlayerInput()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        velocity = new Vector3(horizontalInput, verticalInput, 0f) * moveSpeed;

        if (horizontalInput > 0 && !isFacingRight)
        {
            FlipSprite();
        }
        else if (horizontalInput < 0 && isFacingRight)
        {
            FlipSprite();
        }
    }

    void Move()
    {
        rb.velocity = new Vector3(velocity.x, rb.velocity.y, 0); //Horizontal input only applies directly to player.

        rb.AddForce(new Vector3(rb.velocity.x, velocity.y * 4 )); //Veritcal input applies a force, multiple by 4 to balance out mass and drag.

        animator.SetFloat("Speed", Mathf.Abs(rb.velocity.x + rb.velocity.y));

        //rb.velocity = velocity; //Arcade Feel
        
        //rb.AddForce(velocity);
    }

    public bool IsFacingRight
    {
        get
        {
            return isFacingRight;
        }
    }

    void FlipSprite()
    {
        isFacingRight = !isFacingRight;

        // Multiply the player's x local scale by -1
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

}
