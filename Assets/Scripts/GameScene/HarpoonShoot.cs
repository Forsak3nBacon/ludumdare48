using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarpoonShoot : MonoBehaviour
{
    [SerializeField] float shootSpeed = 5f;
    [SerializeField] float reloadTime = 2f;

    [SerializeField] GameObject harpoonPrefab = null;
    [SerializeField] PlayerMove playerScript = null;
    [SerializeField] GameObject harpoonSpawn = null;

    [SerializeField] GameObject harpoonLoadedSprite = null;
    [SerializeField] GameObject harpoonUnLoadedSprite = null;

    [SerializeField] AudioSource harpoonShootSFX;

    bool shotReady = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (shotReady)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Shoot();
            }
        }
        
    }

    void Shoot()
    {
        shotReady = false;

        harpoonLoadedSprite.SetActive(false);
        harpoonUnLoadedSprite.SetActive(true);


        GameObject firedHarpoon = Instantiate(harpoonPrefab);

        harpoonShootSFX.Play();

        firedHarpoon.transform.position = harpoonSpawn.transform.position;

        Vector3 velocity;

        float localShootSpeed;

        if (!playerScript.IsFacingRight)
        {
            localShootSpeed = -shootSpeed;
            firedHarpoon.GetComponent<Harpoon>().FlipSprite();
        }
        else
        {
            localShootSpeed = shootSpeed;
        }

        velocity = new Vector3(localShootSpeed, 0);

        firedHarpoon.GetComponent<Rigidbody2D>().AddForce(velocity, ForceMode2D.Impulse);

        StartCoroutine(Reload());
    }

    IEnumerator Reload()
    {
        yield return new WaitForSeconds(reloadTime);
        harpoonLoadedSprite.SetActive(true);
        harpoonUnLoadedSprite.SetActive(false);


        shotReady = true;

    }
}
