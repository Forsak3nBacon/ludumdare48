using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContactDamage : MonoBehaviour
{
    [SerializeField] float damage = 1f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        print("Colliding with " + collision.gameObject.name);
        if (collision.gameObject.CompareTag("Player")) //If hit bounding walls without dying, then delete it.
        {
            DealDamage(collision.gameObject);
        }
    }

    void DealDamage(GameObject obj)
    {
        obj.GetComponent<Health>().TakeDamage(damage);

        Destroy(gameObject.transform.parent.gameObject); //Delete the parent on damage
    }
}
