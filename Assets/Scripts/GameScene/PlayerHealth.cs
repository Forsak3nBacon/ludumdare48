using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    [SerializeField] Animator playerAnimator = null; //Only needed on player.
    [SerializeField] AudioSource damageSFX;
    [SerializeField] ParticleSystem bloodParticles;

    public override bool TakeDamage(float damage)
    {
        bool returnResult = base.TakeDamage(damage) ;

        playerAnimator.SetTrigger("Damage");
        damageSFX.Play();
        bloodParticles.Play();

        if (healthCurrent <= 0)
        {
            print(this.gameObject + " is dead.");

            StaticStorage.winState = WinState.Dead;

            return true; //Returning true tells the caller that it's dead.
        }

        return returnResult;
    }
}
