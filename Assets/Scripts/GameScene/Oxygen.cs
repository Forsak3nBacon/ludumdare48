using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Oxygen : MonoBehaviour
{
    [SerializeField] float oxygenMax = 20f;
    [SerializeField] Slider slider = null;
    [SerializeField] AudioSource oxygenPickupSFX;
    [SerializeField] AudioSource oxygenWarningSFX;


    float oxygenCurrent;

    // Start is called before the first frame update
    void Start()
    {
        oxygenCurrent = oxygenMax;
        slider.maxValue = oxygenMax;
        slider.value = oxygenCurrent;
    }

    // Update is called once per frame
    void Update()
    {
        ReduceOxygenTimer();
        UpdateOxygenLevel();
        WarningNoise();
    }

    void ReduceOxygenTimer()
    {
        if (oxygenCurrent > 0)
        {
            oxygenCurrent -= Time.deltaTime;
        }
        else
        {
            oxygenCurrent = 0;

            StaticStorage.winState = WinState.OutOfAir; //Used by GameOver screen

            //gameManager.EndGame(); //You lose
        }
    }

    void UpdateOxygenLevel()
    {
        slider.value = oxygenCurrent;

        if(oxygenCurrent < 0)
        {
            slider.value = 0;
        }
    }

    public void IncreaseOxygen(float oxygen) //If called will add oxygen to the current level.
    {
        oxygenCurrent += oxygen;

        if (oxygenCurrent > oxygenMax) //To prevent the slider going over the max.
        {
            oxygenCurrent = oxygenMax;
        }

        oxygenPickupSFX.Play();

        print("Oxygen is now " + oxygenCurrent);
    }

    void WarningNoise()
    {
        if(oxygenCurrent < 5)
        {
            oxygenWarningSFX.mute = false;
        }
        else
        {
            oxygenWarningSFX.mute = true;
        }
    }
}
