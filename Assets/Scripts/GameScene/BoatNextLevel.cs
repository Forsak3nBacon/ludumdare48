using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatNextLevel : MonoBehaviour
{
    [SerializeField] Score score;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            print("Boat colliding with player");
            if (score.ScoreReached) //Ends the level
            {
                StaticStorage.stagesComplete++;

                StaticStorage.winState = WinState.ScoreAchieved;
            }
        }
    }
}
