using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerNotificationHandler : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI displayText = null;
    [SerializeField] string StartupMessage = "";

    // Start is called before the first frame update
    void Start()
    {
        displayText.enabled = false;

        DisplayString(StartupMessage);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayString(string textToDisplay)
    {
        displayText.text = textToDisplay;
        displayText.enabled = true;

        StartCoroutine(DelayedHide());
    }

    IEnumerator DelayedHide()
    {
        yield return new WaitForSeconds(8);
        displayText.enabled = false;
    }
}
