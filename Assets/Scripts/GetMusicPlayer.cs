using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetMusicPlayer : MonoBehaviour
{
    [SerializeField] Button button;
    MusicPlayer musicPlayer;

    // Start is called before the first frame update
    void Start()
    {
        GameObject musicPlayerObj = GameObject.Find("MusicPlayer");

        if(musicPlayerObj != null)
        {
            musicPlayer = musicPlayerObj.GetComponent<MusicPlayer>();
            button.onClick.AddListener(delegate { ToggleMusic(); });
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void ToggleMusic()
    {
        musicPlayer.ToggleMusic();
    }
}
