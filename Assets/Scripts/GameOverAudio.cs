using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverAudio : MonoBehaviour
{
    [SerializeField] AudioSource loseSFX;
    [SerializeField] AudioSource winSFX;


    // Start is called before the first frame update
    void Start()
    {
        if (StaticStorage.winState == WinState.OutOfAir)
        {
            loseSFX.Play();
        }
        else if (StaticStorage.winState == WinState.AllStagesComplete)
        {
            winSFX.Play();
        }
        else if (StaticStorage.winState == WinState.Dead)
        {
            loseSFX.Play();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
